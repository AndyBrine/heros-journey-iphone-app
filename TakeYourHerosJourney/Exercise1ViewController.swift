//
//  Exercise1ViewController.swift
//  TakeYourHerosJourney
//
//  Created by user121210 on 8/24/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class Exercise1ViewController: UIViewController {
    
    let avPlayerViewController = AVPlayerViewController()
    var avPlayer:AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let movieURL:URL? = URL(string: "https://www.theentrepreneursjourney.tv/AppVideos/Exercise1TheOriginalWorld.m4v")
        
        if let url = movieURL{
            
            self.avPlayer = AVPlayer(url: url)
            self.avPlayerViewController.player = self.avPlayer
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func exercise1PlayButton(_ sender: UIButton) {
        
        //Trigger the video to play
        self.present(self.avPlayerViewController, animated: true) {() -> Void in self.avPlayerViewController.player?.play()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
