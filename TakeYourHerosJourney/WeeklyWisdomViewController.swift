//
//  WeeklyWisdomViewController.swift
//  TakeYourHerosJourney
//
//  Created by user121210 on 8/22/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds

class WeeklyWisdomViewController: UIViewController, GADBannerViewDelegate {

    @IBOutlet var bannerView: GADBannerView!
    //@IBOutlet var youtubePlayer: YTPlayerView!
    @IBOutlet var youtubePlayer: YTPlayerView!
    //@IBOutlet weak var weeklyWisdomTitle: UILabel!
    @IBOutlet var weeklyWisdomTitle: UILabel!
    
    let rootRef = FIRDatabase.database().reference()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3422174915782377/2204194047"
        bannerView.rootViewController = self
        bannerView.load(request)
        
        let videoIdRef = rootRef.child("WeeklyWisdomVideo")
        
        let playerVars = ["playsinline":"1", "fs":"0"]
        
        videoIdRef.observe(.value) { (snap: FIRDataSnapshot) in
            //self.youtubePlayer.loadWithVideoId((snap.value?.description)!)
            self.youtubePlayer.load(withVideoId: snap.value as! String!, playerVars: playerVars)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            
        /*let weeklyWisdomTitleRef = FIRDatabase.database().reference().child("WeeklyWisdomTitle")
        weeklyWisdomTitleRef.observe(.value) { (snap: FIRDataSnapshot) in
            self.weeklyWisdomTitle.text = (snap.value as AnyObject).description
        }*/
        let weeklyWisdomTitleRef = FIRDatabase.database().reference().child("WeeklyWisdomTitle")
        weeklyWisdomTitleRef.observe(.value) { (snap: FIRDataSnapshot) in
            self.weeklyWisdomTitle.text = snap.value as? String
        }
    }
    
    @IBAction func gotoYouTubeChannel(_ sender: UIButton) {
        if let url = URL(string: "https://www.youtube.com/channel/UCMy19TLQcVOFxAIQRjMEpQQ"){
            UIApplication.shared.openURL(url)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
