//
//  ClarityCallViewController.swift
//  TakeYourHerosJourneyPRO
//
//  Created by user121210 on 8/26/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit

class ClarityCallViewController: UIViewController {

    @IBOutlet var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.isScrollEnabled = false;
        textView.isScrollEnabled = true;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func bookYourClarityCall(_ sender: UIButton) {
        if let url = URL(string: "https://calendly.com/andybrine/herosjourneyclaritycall"){
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = false;
    }
    
    override func viewWillDisappear(_ animated: Bool){
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true;
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
