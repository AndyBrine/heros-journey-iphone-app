//
//  Exercise8ViewController.swift
//  TakeYourHerosJourney
//
//  Created by user121210 on 8/25/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SDWebImage
import Firebase

class UpcomingEventsViewController: UIViewController {
    
    @IBOutlet var ImageView: UIImageView!
    let rootRef = FIRDatabase.database().reference()
    var eventUrl: String?
    
    override func viewDidLoad() {      super.viewDidLoad()
        
        let imageUrl:URL? = URL(string: "https://www.theentrepreneursjourney.tv/AppVideos/EventPromoPhone.png")
        
        if let url = imageUrl{
            ImageView.sd_setImage(with: url as URL!)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /*let weeklyWisdomTitleRef = FIRDatabase.database().reference().child("WeeklyWisdomTitle")
         weeklyWisdomTitleRef.observe(.value) { (snap: FIRDataSnapshot) in
         self.weeklyWisdomTitle.text = (snap.value as AnyObject).description
         }*/
        let weeklyWisdomTitleRef = FIRDatabase.database().reference().child("EventUrl")
        weeklyWisdomTitleRef.observe(.value) { (snap: FIRDataSnapshot) in
            self.eventUrl = snap.value as? String
        }
    }
    
    @IBAction func moreInformationButton(_ sender: UIButton) {
        if let url = URL(string: eventUrl!){
            UIApplication.shared.openURL(url as URL)
        }
    }
}
