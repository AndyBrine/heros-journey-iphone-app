//
//  Stage9ViewController.swift
//  TakeYourHerosJourney
//
//  Created by user121210 on 8/21/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import GoogleMobileAds

class Stage9ViewController: UIViewController,  GADBannerViewDelegate {
    
    @IBOutlet var bannerView: GADBannerView!
    let avPlayerViewController = AVPlayerViewController()
    var avPlayer:AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let movieURL:URL? = URL(string: "https://www.theentrepreneursjourney.tv/AppVideos/Stage9TheReward.m4v")
        
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        bannerView.delegate = self
        bannerView.adUnitID = "ca-app-pub-3422174915782377/2204194047"
        bannerView.rootViewController = self
        bannerView.load(request)
        
        if let url = movieURL{
            
            self.avPlayer = AVPlayer(url: url)
            self.avPlayerViewController.player = self.avPlayer
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        
        self.present(self.avPlayerViewController, animated: true) {() -> Void in self.avPlayerViewController.player?.play()
        }
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = true;
    }
    
    override func viewWillDisappear(_ animated: Bool){
        self.tabBarController?.moreNavigationController.isNavigationBarHidden = false;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
