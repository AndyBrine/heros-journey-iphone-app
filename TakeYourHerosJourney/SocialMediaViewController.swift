//
//  SocialMediaViewController.swift
//  TakeYourHerosJourney
//
//  Created by user121210 on 8/24/16.
//  Copyright © 2016 Thorn Technologies, LLC. All rights reserved.
//

import UIKit

class SocialMediaViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func facebookLinkButton(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.facebook.com/TheEntrepreneursJourney/?pnref=lhc"){
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func linkedInLinkButton(_ sender: UIButton) {
        if let url = URL(string: "https://uk.linkedin.com/in/andybrine"){
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func twitterLinkButton(_ sender: UIButton) {
        if let url = URL(string: "https://twitter.com/AndyBrineUk"){
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func instagramLinkButton(_ sender: UIButton) {
        if let url = URL(string: "https://www.instagram.com/andybrineuk/"){
            UIApplication.shared.openURL(url)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
